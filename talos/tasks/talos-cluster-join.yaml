---
Description: A task to join a talos cluster
Documentation: |
  This task join a talos cluster.
Meta:
  color: yellow
  feature-flags: sane-exit-codes
  icon: linux
  type: os
Name: talos-cluster-join
ExtraClaims:
  - action: '*'
    scope: 'zones'
    specific: '*'
  - action: '*'
    scope: 'machines'
    specific: '*'
  - action: '*'
    scope: 'profiles'
    specific: '*'
OptionalParams: []
Prerequisites: []
RequiredParams: []
Templates:
- Contents: |
    #!/usr/bin/env bash

    {{ template "setup.tmpl" . }}

    n_cp_req={{ .Param "talos/controlplanes/names" | len }}

    while true ; do
        cp=$(drpcli machines list 'Profiles=Eq({{.Machine.Name}})' 'Params.talos/role=Eq(controlplane)')
        n_cp_act=$(jq '[.[] .Address| select(. != "")] | length' <<< $cp)
        job_info "Waiting for $n_cp_act/$n_cp_req controlplanes to be ready"
        if [[ n_cp_act -eq n_cp_req ]] ; then
            break
        fi
        sleep 5
    done

    cp_ips=($(jq '.[].Address' -r <<< $cp))
    BOOTSTRAP_IP="${cp_ips[0]}"
    job_info "Bootstrap IP is $BOOTSTRAP_IP"

    job_info "Updating talosconfig with endpoint starting point"
    talosctl --talosconfig /root/talosconfig config endpoint $BOOTSTRAP_IP
    job_info "Updating talosconfig with node starting point"
    talosctl --talosconfig /root/talosconfig config node $BOOTSTRAP_IP

    # This script is also called when adding new nodes to an existing cluster, no need to bootstrap.
    kubeconfig=$(drpcli profiles get {{.Machine.Name}} param talos/kubeconfig)
    if [[ $kubeconfig != null ]]; then
      job_info "Cluster {{.Machine.Name}} already bootstrapped"
      exit 0
    fi

    # Set Names into DNS ZONE
    cat >zone1.yaml <<EOF
    Description: '{{.Machine.Name}} DNS ZONE'
    Meta:
      color: black
      icon: draw polygon
      title: Cluster added
    Name: {{.Machine.Name}}
    Origin: {{.Machine.Name}}.cluster.
    Priority: 0
    ReadOnly: false
    TTL: 3600
    Records:
    EOF

    for ip in ${cp_ips[@]} ; do
      echo "Adding $ip"
      cat >>zone1.yaml <<EOF
    - Name: k8s
      Type: A
      Value:
        - $ip
    EOF
    done

    job_info "zone config:"
    cat zone1.yaml

    drpcli zones destroy {{.Machine.Name}} >/dev/null 2>/dev/null || true
    drpcli zones create zone1.yaml >/dev/null

    while true ; do
        job_info "Waiting on cluster members"
        if talosctl get members --talosconfig /root/talosconfig | grep -q "$BOOTSTRAP_IP" ; then
            break
        fi
        sleep 5
    done


    job_info "Bootstrapping cluster"
    talosctl bootstrap --talosconfig /root/talosconfig

    job_info "Replace the config"
    drpcli profiles set {{.Machine.Name}} param talos/config to - < /root/talosconfig

    job_info "Get k8s config file"
    talosctl kubeconfig ./kubeconfig --talosconfig /root/talosconfig

    job_info "Store k8s config file"
    drpcli profiles set {{.Machine.Name}} param talos/kubeconfig to - < ./kubeconfig

    exit 0
  Name: talos-cluster-join.sh
  Path: ""
